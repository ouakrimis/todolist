package com.toDoList.Repository;

import com.toDoList.Entity.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;


@RepositoryRestResource
public interface ListRepo extends PagingAndSortingRepository<List, Long>
{
		@RestResource(path="byName") 
		public List findByName(@Param("name") String listName);
}

