package com.toDoList.Repository;

import com.toDoList.Entity.Task;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource
public interface TaskRepo extends PagingAndSortingRepository<Task, Long>
{
		@RestResource(path="byName") 
		public Task findByName(@Param("name") String taskName);
}

