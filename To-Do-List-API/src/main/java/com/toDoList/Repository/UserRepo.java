package com.toDoList.Repository;

import com.toDoList.Entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource
public interface UserRepo extends PagingAndSortingRepository<User, Long>
{
		@RestResource(path="byEmail")
		public User findByEmail(@Param("email") String email);
}

