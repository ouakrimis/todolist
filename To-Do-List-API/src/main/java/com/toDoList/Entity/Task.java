package com.toDoList.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name="task")
public class Task implements Serializable
{

	private static final long serialVersionUID = -53783965437316545L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@Column
	private Boolean isDone;

	@Column
	private Date createdAt;

	@Column
	private Date dueDate;

	public Long getid() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDone() {
		return isDone;
	}

	public void setDone(Boolean done) {
		isDone = done;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Task task = (Task) o;

		if (!id.equals(task.id)) return false;
		if (!name.equals(task.name)) return false;
		if (!isDone.equals(task.isDone)) return false;
		if (!createdAt.equals(task.createdAt)) return false;
		return dueDate.equals(task.dueDate);

	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + isDone.hashCode();
		result = 31 * result + createdAt.hashCode();
		result = 31 * result + dueDate.hashCode();
		return result;
	}
}
