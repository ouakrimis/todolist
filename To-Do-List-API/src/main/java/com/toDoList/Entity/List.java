package com.toDoList.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.Date;

@Entity(name="list")
public class List implements Serializable
{

	private static final long serialVersionUID = -537839637337316545L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@Column
	private Boolean isPublic;

	@Column
	private Date createdAt;


	@OneToOne(fetch = FetchType.LAZY)
	private User createdBy;

	@OneToMany(fetch = FetchType.LAZY)
	private Set<Task> tasks;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getPublic() {
		return isPublic;
	}

	public void setPublic(Boolean aPublic) {
		isPublic = aPublic;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		List list = (List) o;

		if (!id.equals(list.id)) return false;
		if (!name.equals(list.name)) return false;
		if (!isPublic.equals(list.isPublic)) return false;
		return createdAt.equals(list.createdAt);

	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + isPublic.hashCode();
		result = 31 * result + createdAt.hashCode();
		return result;
	}
}
